package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        start:

        while(true) {
            System.out.println("Let's play round " + roundCounter++);
            System.out.println("Your choice (Rock/Paper/Scissors)? ");
            String humanMove = sc.nextLine();

            if(!humanMove.equals("rock") && !humanMove.equals("paper") && !humanMove.equals("scissors")) {
                System.out.println("I don't understand " + humanMove + ". Try again");

            } else {

                int rand = (int)(Math.random()*3);
                String computerMove = " ";
                if (rand == 0) {
                    computerMove = "rock";
                } else if (rand == 1) {
                    computerMove = "paper";
                } else {
                    computerMove = "scissors";
                }


            if (humanMove.equals(computerMove)) {
                System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". It's a tie!");
            } else if(humanMove.equals("rock") && computerMove.equals("scissors") ||
             (humanMove.equals("paper") && computerMove.equals("rock")) ||
            (humanMove.equals("scissors") && computerMove.equals("paper"))) {
            System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". Human won!");
            humanScore++;
            
            }else {
                System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". Computer won!");
                computerScore++;
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                
                while(true) {
                    System.out.println("Do you wish to continue playing? (y/n)? ");
                    String newRound = sc.nextLine();
                    if(newRound.equals("y")) {
                        break;

                    } else if(!newRound.equals("y") && !newRound.equals("n")) {
                        System.out.println("I don't understand" + newRound + ". Try again.");

                    }else {
                        System.out.println("Bye bye :)");
                        break start;
                    }
}            
}            
}
}

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
